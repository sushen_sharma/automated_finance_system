import PySimpleGUI as gui
from datetime import  date
from plotly.subplots import make_subplots
from Trading_Setup_GUI.Folder_operations import  Folder_Operation
import plotly.graph_objects as go
import pandas as pd
from win32api import GetSystemMetrics
import datetime, calendar
from pandas import  DataFrame
import re
from Trading_Setup_GUI.Option_Data_Processing import Option_Data

class Business_Logics:

    def Future_chart(self):
        fig = make_subplots(rows=2, cols=1,shared_xaxes=True,vertical_spacing=0.00)
        df = pd.read_csv('D:\FObhavCopy_Data\AppleData.csv')
        df_1 = pd.read_csv('D:\FObhavCopy_Data\AppleData.csv')
        df_1 = df_1[1:80]
        x = list(df_1['Date'])
        y = list(df_1['AAPL.Volume'])


        fig.add_trace(go.Candlestick(x=df_1['Date'],
                                     open=df['AAPL.Open'],
                                     high=df['AAPL.High'],
                                     low=df['AAPL.Low'],
                                     close=df['AAPL.Close']))

        fig.update_layout(xaxis_rangeslider_visible=False)

        fig.add_trace(
            go.Bar(x=x, y=y),
            row=2, col=1
        )

        fig.update_layout(height=GetSystemMetrics(1), width=GetSystemMetrics(0), title_text="Future OI vs Volume Chart")
        fig.show()

    def Get_the_List_Of_Stocks(self):
        df_stock_list = pd.read_csv(Folder_Operation.stock_list_path)
        df_stock_list =df_stock_list[(df_stock_list['INSTRUMENT'] == 'FUTSTK')]
        total_list = list(df_stock_list['SYMBOL'])
        stock_list = []
        for i in total_list:
            if i not in stock_list:
                stock_list.append(i)
            stock_list.sort()
        return stock_list

    def Get_the_List_Of_MidCap_Stocks(self):
        df_stock_list = pd.read_csv(Folder_Operation.Nifty_400_stock_list_path)

        total_list = list(df_stock_list['Symbol'])
        stock_list = []
        for i in total_list:
            if i not in stock_list:
                stock_list.append(i)
            stock_list.sort()
        return stock_list


    def Find_Expiry_Date_For_Current_Month(self,year,month):
        # Create a datetime.date for the last day of the given month
        daysInMonth = calendar.monthrange(year, month)[1]  # Returns (month, numberOfDaysInMonth)
        dt = datetime.date(year, month, daysInMonth)

        # Back up to the most recent Thursday
        offset = 4 - dt.isoweekday()
        if offset > 0: offset -= 7  # Back up one week if necessary
        dt += datetime.timedelta(offset)  # dt is now date of last Th in month

        # Throw an exception if dt is in the current month and occurred before today
        now = datetime.date.today()  # Get current date (local time, not utc)
        if dt.year == now.year and dt.month == now.month and dt < now:
            raise Exception('Oops - missed the last Thursday of this month')
        x = datetime.datetime(dt.year,dt.month,dt.day)
        dt = x.strftime("%d-%b-%Y").upper()
        return dt

    def Process_Future_data_for_stock(self,stock_symbol,FO_Files_list,expiry_Date_For_Current_Month,expiry_Date_For_Previous_Month):
      if stock_symbol in self.Get_the_List_Of_Stocks():
            print("available")
            MainDataFrame = []
            for filename in FO_Files_list:
                dataFrame = pd.read_csv(filename)
                dataFrame_Consolidated_COI = dataFrame[(dataFrame['SYMBOL'] == stock_symbol.upper()) & ((dataFrame['INSTRUMENT'] == 'FUTSTK'))]
                dataFrame["Cumlative COI"] = dataFrame_Consolidated_COI['OPEN_INT'].sum()
                MainDataFrame.append((dataFrame))
            dataFrame = pd.concat(MainDataFrame)
            dataFrame = dataFrame[((dataFrame['EXPIRY_DT'] == expiry_Date_For_Current_Month)|(dataFrame['EXPIRY_DT'] == expiry_Date_For_Previous_Month)) & (dataFrame['INSTRUMENT'] == 'FUTSTK')]
            dataFrame = dataFrame[dataFrame['SYMBOL'] == stock_symbol.upper()]
            dataFrame['PO_Ratio'] = dataFrame['CHG_IN_OI']/dataFrame['CONTRACTS']
            dataFrame['ClOP'] = dataFrame['CLOSE']-dataFrame['OPEN']
            dataFrame['HiLo'] = dataFrame['HIGH'] - dataFrame['LOW']
            dataFrame['Wick'] = (abs(abs(dataFrame['HiLo']) - abs(dataFrame['ClOP'])))
            dataFrame['ClOP_per'] = (100 - (((dataFrame['CLOSE'] - dataFrame['ClOP']) * 100) / dataFrame['CLOSE']))
            dataFrame['Wick_Per_On_Spot'] = (100 - (((dataFrame['CLOSE'] - dataFrame['Wick'])*100)/dataFrame['CLOSE']))
            dataFrame['HiLo_Percentage'] = ((dataFrame['HIGH']-dataFrame['LOW'])*100)/dataFrame['HIGH']
            dataFrame['vol_dis'] = dataFrame['CONTRACTS']/dataFrame['HiLo_Percentage']
            dataFrame['Alert'] = ""
            dataFrame.loc[(dataFrame['ClOP_per'] < dataFrame['Wick_Per_On_Spot']), ['Alert']] = "HIGH"

            dataFrame['vol_dis_old'] = dataFrame['vol_dis'].shift(1, fill_value=0, axis=0)
            dataFrame['Per_vol_Updown'] = ((dataFrame['vol_dis'] - dataFrame['vol_dis_old'])*100)/dataFrame['vol_dis']
            dataFrame['x_factor'] = (abs(abs(dataFrame['Wick_Per_On_Spot'])/abs(dataFrame['ClOP_per'])))
            dataFrame['COI_Percentage'] = (dataFrame['CHG_IN_OI']/dataFrame['OPEN_INT'])*100
            dataFrame["vol_dis"] = ((dataFrame["vol_dis"] / dataFrame["vol_dis"].mean()) * 100)


            dataFrame = dataFrame.round(2)
            data = []
            for index, row in dataFrame.iterrows():
                subdata = []
                subdata.append(row['TIMESTAMP'])
                subdata.append(row['CONTRACTS'])
                subdata.append(row['CLOSE'])
                subdata.append(row['vol_dis'])
                subdata.append(row['PO_Ratio'])
                subdata.append(row['CHG_IN_OI'])
                subdata.append(row['Cumlative COI'])
                data.append(subdata)
            return data
      else:
          data =[]
          data.append([0,0,0,0,0,0,0])

          return  data

    def Process_Cash_data_for_stock(self, stock_symbol, Cash_Files_list):
        MainDataFrame_cash = []
        for filename in Cash_Files_list:
            dataFrame_cash = pd.read_csv(filename)

            MainDataFrame_cash.append((dataFrame_cash))
        dataFrame_cash = pd.concat(MainDataFrame_cash)

        dataFrame_cash = dataFrame_cash[dataFrame_cash['SYMBOL'] == stock_symbol.upper()]

        dataFrame_cash[' DELIV_PER'] = dataFrame_cash[' DELIV_PER'].astype(float)
        dataFrame_cash[' DELIV_QTY'] = dataFrame_cash[' DELIV_QTY'].astype(float)

        dataFrame_cash['CLO']=dataFrame_cash[' CLOSE_PRICE'] - dataFrame_cash[' OPEN_PRICE']
        dataFrame_cash['HiLo'] = dataFrame_cash[' HIGH_PRICE'] - dataFrame_cash[' LOW_PRICE']
        dataFrame_cash['Wick'] = (abs(abs(dataFrame_cash['HiLo']) - abs(dataFrame_cash['CLO'])))
        dataFrame_cash['ClOP_per'] = (100 - (((dataFrame_cash[' CLOSE_PRICE'] - dataFrame_cash['CLO']) * 100) / dataFrame_cash[' CLOSE_PRICE']))

        dataFrame_cash['Wick_Percentage'] = (100 - (((dataFrame_cash[' CLOSE_PRICE'] - dataFrame_cash['Wick']) * 100) / dataFrame_cash[' CLOSE_PRICE']))
        dataFrame_cash['HiLo_Percentage'] = ((dataFrame_cash[' HIGH_PRICE'] - dataFrame_cash[' LOW_PRICE']) * 100) / dataFrame_cash[' HIGH_PRICE']
        dataFrame_cash['vol_dis'] = dataFrame_cash[' TTL_TRD_QNTY'] / dataFrame_cash['HiLo_Percentage']
        dataFrame_cash['Alert'] = ""

        dataFrame_cash.loc[(dataFrame_cash['ClOP_per'] < dataFrame_cash['Wick_Percentage']), ['Alert']] = "HIGH"
        dataFrame_cash['vol_dis_old'] = dataFrame_cash['vol_dis'].shift(1, fill_value=0, axis=0)
        dataFrame_cash['Per_vol_Updown'] = ((dataFrame_cash['vol_dis'] - dataFrame_cash['vol_dis_old']) * 100) / dataFrame_cash['vol_dis']
        dataFrame_cash['Factor'] = (abs(abs(dataFrame_cash['Wick_Percentage']) / abs(dataFrame_cash['ClOP_per'])))
        dataFrame_cash["Factor"] = ((dataFrame_cash["Factor"] / dataFrame_cash["Factor"].mean()) * 100)

        dataFrame_cash['delivery_Factor'] = dataFrame_cash[' DELIV_QTY'] / dataFrame_cash['HiLo']
        dataFrame_cash['delivery_Factor_old'] = dataFrame_cash['delivery_Factor'].shift(1, fill_value=0, axis=0)
        dataFrame_cash['delivery_density'] = ((dataFrame_cash['delivery_Factor'] - dataFrame_cash['delivery_Factor_old']) * 100) / dataFrame_cash['delivery_Factor']
        dataFrame_cash["delivery_Factor"] = ((dataFrame_cash["delivery_Factor"]/dataFrame_cash["delivery_Factor"].mean()) * 100)

        dataFrame_cash = dataFrame_cash.round(2)



        data = []
        for index, row in dataFrame_cash.iterrows():
            subdata = []
            subdata.append(row[' DATE1'])
            subdata.append(row['vol_dis'])
            subdata.append(row[' CLOSE_PRICE'])
            subdata.append(row['Factor'])
            subdata.append(row['delivery_Factor'])
            subdata.append(row['delivery_density'])
            subdata.append(row[' DELIV_QTY'])
            subdata.append(row[' NO_OF_TRADES'])
            data.append(subdata)
        return data

    def Process_Option_data_for_stock(self, stock_symbol, FO_Files_list, expiry_Date_For_Current_Month):
        if stock_symbol in self.Get_the_List_Of_Stocks():
            print("available")
            MainDataFrame = []
            for filename in FO_Files_list:
                dataFrame = pd.read_csv(filename)
                dataFrame_Consolidated_COI = dataFrame[(dataFrame['SYMBOL'] == stock_symbol.upper()) & ((dataFrame['INSTRUMENT'] == 'OPTSTK'))]

                MainDataFrame.append((dataFrame))
            dataFrame = pd.concat(MainDataFrame)
            dataFrame = dataFrame[((dataFrame['EXPIRY_DT'] == expiry_Date_For_Current_Month)) & (dataFrame['INSTRUMENT'] == 'OPTSTK')]
            dataFrame = dataFrame[dataFrame['SYMBOL'] == stock_symbol.upper()]


            data = []
            for index, row in dataFrame.iterrows():
                subdata = []
                subdata.append(row['TIMESTAMP'])
                subdata.append(row['SYMBOL'])
                subdata.append(row['EXPIRY_DT'])
                subdata.append(row['STRIKE_PR'])
                subdata.append(row['OPTION_TYP'])
                subdata.append(row['OPEN'])
                subdata.append(row['HIGH'])
                subdata.append(row['LOW'])
                subdata.append(row['CLOSE'])
                subdata.append(row['CONTRACTS'])
                subdata.append(row['VAL_INLAKH'])
                subdata.append(row['OPEN_INT'])
                subdata.append(row['CHG_IN_OI'])
                data.append(subdata)
            return data
        else:
            data = []
            data.append([0, 0, 0, 0, 0, 0, 0,0,0,0,0,0,0])

            return data

    def Convert_List_To_Dataframe(self,stock_data_list,stock_name):


        # list_of_list_appended_with_StockName = []
        # for item in stock_data_list:
        #     item.append(stock_name)
        #     list_of_list_appended_with_StockName.append(item)
        #     item = []
        sub_dataFrame = DataFrame(stock_data_list,columns=["SYMBOL", "EXPIRY_DT", "STRIKE_PR", "PO_Ratio","CHG_IN_OI","Cumlative COI","CONTRACTS","CLOSE", "vol_dis","COI_Percentage","Per_vol_Updown","TIMESTAMP"])
        return sub_dataFrame

    def Process_Future_data_for_All_Stocks_And_Make_DataFrame(self,stock_symbol,FO_Files_list,expiry_Date_For_Current_Month):
        MainDataFrame = []
        for filename in FO_Files_list:
            dataFrame = pd.read_csv(filename)
            dataFrame_Consolidated_COI = dataFrame[(dataFrame['SYMBOL'] == stock_symbol.upper()) & ((dataFrame['INSTRUMENT'] == 'FUTSTK'))]
            dataFrame["Cumlative COI"] = dataFrame_Consolidated_COI['CHG_IN_OI'].sum()
            MainDataFrame.append((dataFrame))
        dataFrame = pd.concat(MainDataFrame)

        dataFrame = dataFrame[(dataFrame['EXPIRY_DT'] == expiry_Date_For_Current_Month) & (dataFrame['INSTRUMENT'] == 'FUTSTK')]
        dataFrame = dataFrame[dataFrame['SYMBOL'] == stock_symbol.upper()]
        dataFrame['PO_Ratio'] = dataFrame['CHG_IN_OI']/dataFrame['CONTRACTS']
        dataFrame['ClOP'] = dataFrame['CLOSE']-dataFrame['OPEN']
        dataFrame['HiLo'] = dataFrame['HIGH'] - dataFrame['LOW']
        dataFrame['Wick'] = (abs(abs(dataFrame['HiLo']) - abs(dataFrame['ClOP'])))
        dataFrame['ClOP_per'] = (100 - (((dataFrame['CLOSE'] - dataFrame['ClOP']) * 100) / dataFrame['CLOSE']))
        dataFrame['Wick_Per_On_Spot'] = (100 - (((dataFrame['CLOSE'] - dataFrame['Wick'])*100)/dataFrame['CLOSE']))
        dataFrame['HiLo_Percentage'] = ((dataFrame['HIGH']-dataFrame['LOW'])*100)/dataFrame['HIGH']
        dataFrame['vol_dis'] = dataFrame['CONTRACTS']/dataFrame['HiLo_Percentage']
        dataFrame['Alert'] = ""
        dataFrame.loc[(dataFrame['ClOP_per'] < dataFrame['Wick_Per_On_Spot']), ['Alert']] = "HIGH"

        dataFrame['vol_dis_old'] = dataFrame['vol_dis'].shift(1, fill_value=0, axis=0)
        dataFrame['Per_vol_Updown'] = ((dataFrame['vol_dis'] - dataFrame['vol_dis_old'])*100)/dataFrame['vol_dis']


        dataFrame['x_factor'] = (abs(abs(dataFrame['Wick_Per_On_Spot'])/abs(dataFrame['ClOP_per'])))
        dataFrame['COI_Percentage'] = (dataFrame['CHG_IN_OI']/dataFrame['OPEN_INT'])*100
        dataFrame["vol_dis"] = ((dataFrame["vol_dis"] / dataFrame["vol_dis"].mean()) * 100)


        dataFrame = dataFrame.round(2)

        data = []
        for index, row in dataFrame.iterrows():
            subdata = []
            subdata.append(row['SYMBOL'])
            subdata.append(row['EXPIRY_DT'])
            subdata.append(row['STRIKE_PR'])

            subdata.append(row['PO_Ratio'])
            subdata.append(row['CHG_IN_OI'])
            subdata.append(row['Cumlative COI'])

            subdata.append(row['CONTRACTS'])
            subdata.append(row['CLOSE'])
            subdata.append(row['vol_dis'])
            subdata.append(row['COI_Percentage'])
            subdata.append(row['Per_vol_Updown'])
            subdata.append(row['TIMESTAMP'])
            data.append(subdata)
        return data

    def Get_Today_Date(self):
        today = date.today()
        x = datetime.datetime(today.year, today.month, today.day)
        Today_Date = x.strftime("%d-%b-%Y").upper()
        return Today_Date

    def Get_Month_From_FileName(self,fileName):
        fileName = "D:\FObhavCopy_Data\fo04Jul32020bhav.csv"
        fileName = fileName.upper()
        pattern_First = re.compile(r'\d{2}[A-Z]{3}\d{4}')
        Extracted_Month_Along_withDate_And_Year = pattern_First.findall(fileName)
        pattern_Second = re.compile(r'[A-Z]{3}')
        month = pattern_Second.findall(Extracted_Month_Along_withDate_And_Year[0])[0]
        return month


    def Future_and_Cash_Data(self,FO_Files_list,Cash_Files_List):

        today = date.today()
        x = datetime.datetime(today.year, today.month-1, today.day)
        expiry_Date_For_Current_Month = self.Find_Expiry_Date_For_Current_Month(today.year, today.month)
        expiry_Date_For_Current_Month = expiry_Date_For_Current_Month.replace(expiry_Date_For_Current_Month[3:6],expiry_Date_For_Current_Month[3:6].capitalize())

        expiry_Date_For_Previous_Month = self.Find_Expiry_Date_For_Current_Month(today.year, today.month-1)
        expiry_Date_For_Previous_Month = expiry_Date_For_Current_Month.replace(expiry_Date_For_Current_Month[3:6],expiry_Date_For_Current_Month[3:6].capitalize())

        MainDataFrame = []
        for  filename in FO_Files_list:
             dataFrame = pd.read_csv(filename)
             MainDataFrame.append((dataFrame))
        dataFrame = pd.concat(MainDataFrame)

        dataFrame = dataFrame[((dataFrame['EXPIRY_DT'] == expiry_Date_For_Current_Month) | (dataFrame['EXPIRY_DT'] == expiry_Date_For_Previous_Month) ) & (dataFrame['INSTRUMENT'] == 'FUTSTK')  ]
        dataFrame = dataFrame[['SYMBOL','OPEN','HIGH','LOW','CLOSE','TIMESTAMP']]
        headings_future_Table = ['TIMESTAMP', 'CONTRACTS', 'CLOSE', 'vol_dis %', 'PO_Ratio', 'CHG_IN_OI','Cumlative COI']
        headings_Cash_Table = ['TIMESTAMP', 'vol_dis %', 'CLOSE_PRICE', 'Factor', 'delivery_Factor', 'delivery_density', 'DELIV_QTY','NO_OF_TRADES']
        headings_Option_Table = ['TIMESTAMP', 'SYMBOL', 'EXPIRY_DT', 'STRIKE_PR', 'OPTION_TYP', 'OPEN','HIGH', 'LOW','CLOSE','CONTRACTS', 'VAL_INLAKH','OPEN_INT', 'CHG_IN_OI']


        data = []
        for index,row in dataFrame.iterrows():
            subdata = []
            subdata.append(row['SYMBOL'])
            subdata.append(row['OPEN'])
            subdata.append(row['HIGH'])
            subdata.append(row['LOW'])
            subdata.append(row['CLOSE'])
            subdata.append(row['TIMESTAMP'])
            subdata.append(row['HIGH'])
            subdata.append(row['LOW'])
            subdata.append(row['CLOSE'])
            data.append(subdata)

        raw_option_data = [[0,0,0,0,0,0,0,0,0,0,0,0,0]]


        Table_Panel = [
            [gui.Text("StockName : ", font=('MS Sans Serif', 7, 'bold'), text_color='red', background_color='white'),
             gui.DropDown(values=self.Get_the_List_Of_MidCap_Stocks(),auto_size_text=True,enable_events=True,size=(40,30),),
             gui.Button("Process Stocks Data", font=('MS Sans Serif', 10, 'bold'), button_color=('Blue', 'white'), ),
             # gui.Text("Progress Bar for Data Processing : ", font=('MS Sans Serif', 7, 'bold'), text_color='red',
             #          background_color='white'),
             gui.ProgressBar(len(self.Get_the_List_Of_Stocks()), orientation='h', size=(20, 20), key='progbar'),
             gui.Checkbox('Adjust COI Percentage     ', font=('MS Sans Serif', 7, 'bold'), text_color='red',
                          background_color='white', enable_events=True, key='COI_Percentage_enabled',
                          change_submits=True, default=True),
             gui.InputText('0', key='_COI_INPUT_Value_', size=[5, 1], font=('MS Sans Serif', 7, 'bold'), text_color='red',
                      background_color='white'),
             gui.Checkbox('Adjust Percentage Volume', font=('MS Sans Serif', 7, 'bold'), text_color='red',
                          background_color='white', enable_events=True, key='Percentage_volume_enabled',
                          change_submits=True),
             gui.InputText('0', key='_Percentage_SLIDER_Value_', size=[5, 1], font=('MS Sans Serif', 7, 'bold'),
                      text_color='red', background_color='white'),
             gui.Button("Filter", font=('MS Sans Serif', 10, 'bold'), button_color=('Blue', 'white'))

             ],
            [gui.Button("Future Chart", font=('MS Sans Serif', 10, 'bold'), button_color=('Blue', 'white')),
             gui.Button("Cash Chart", font=('MS Sans Serif', 10, 'bold'), button_color=('Blue', 'white')),
             gui.Button("Option Chart", font=('MS Sans Serif', 10, 'bold'), button_color=('Blue', 'white')),


            ],



            [gui.Table(values=data,
                       headings=headings_future_Table,

                       auto_size_columns=True,
                       justification='right',
                       key='-TABLE_FO-',
                       alternating_row_color='green',
                       num_rows=min(len(data), 20), vertical_scroll_only=False),

             gui.Table(values=data,
                       headings=headings_Cash_Table,
                       auto_size_columns=True,
                       justification='right',
                       key='-TABLE_CASH-',
                       alternating_row_color='red',
                       num_rows=min(len(data), 20), vertical_scroll_only=False),],
            [
                gui.Table(values=raw_option_data,
                       headings=headings_Option_Table,
                       auto_size_columns=True,
                       justification='right',
                       key='-TABLE_OPTION-',
                       alternating_row_color='brown',
                       num_rows=min(len(data), 20), vertical_scroll_only=False)

             ]
        ]
        window_Tabel_FO_Data = gui.Window('Table', Table_Panel, grab_anywhere=False, resizable=True,size=[GetSystemMetrics(0),GetSystemMetrics(1)])

        while True:
            event_Tabel_FO_Data, values = window_Tabel_FO_Data.read()
            if event_Tabel_FO_Data == 0:
                    stock_specific_values_FO = self.Process_Future_data_for_stock(values[0],FO_Files_list,expiry_Date_For_Current_Month,expiry_Date_For_Previous_Month)
                    stock_specific_values_CASH = self.Process_Cash_data_for_stock(values[0], Cash_Files_List)
                    stock_specific_values_OPTION = self.Process_Option_data_for_stock(values[0], FO_Files_list,expiry_Date_For_Current_Month)

                    window_Tabel_FO_Data['-TABLE_FO-'].update(values=stock_specific_values_FO)
                    window_Tabel_FO_Data['-TABLE_CASH-'].update(values=stock_specific_values_CASH)
                    window_Tabel_FO_Data['-TABLE_OPTION-'].update(values=stock_specific_values_OPTION)

            if event_Tabel_FO_Data == "Future Chart":
                    self.Future_chart()

            if event_Tabel_FO_Data == "Cash Chart":
                self.Future_chart()

            if event_Tabel_FO_Data == "Option Chart":
                o = Option_Data()
                o.Option_GUI_Formation()

            if event_Tabel_FO_Data == "Process Stocks Data":
                gui.popup_auto_close("Data Processing Starts . Please wait for the process to complete")
                list_of_Stocks = self.Get_the_List_Of_Stocks()

                Cumlative_DataFrame = []
                i = 0
                for stock_name in list_of_Stocks[0:len(list_of_Stocks)]:
                    i = i + 1
                    window_Tabel_FO_Data['progbar'].update_bar(i)
                    stock_specific_values_FO = self.Process_Future_data_for_All_Stocks_And_Make_DataFrame(stock_name, FO_Files_list,
                                                                                  expiry_Date_For_Current_Month)
                    dataFrame_new = self.Convert_List_To_Dataframe(stock_specific_values_FO, stock_name)
                    Cumlative_DataFrame.append(dataFrame_new)


                data_frames_of_all_Stocks_Post_Data_Processing = pd.concat(Cumlative_DataFrame)
                data_frames_of_all_Stocks_Post_Data_Processing.to_csv("Koko.csv")

                print(data_frames_of_all_Stocks_Post_Data_Processing.info())

            if event_Tabel_FO_Data == "Filter":


                if (values['COI_Percentage_enabled'] & (not values['Percentage_volume_enabled'])):

                    # data_frames_of_all_Stocks_Post_Data_Processing = pd.read_csv("Koko.csv")
                    #print(data_frames_of_all_Stocks_Post_Data_Processing.info())
                    filtered_Data_frame = data_frames_of_all_Stocks_Post_Data_Processing[
                        (data_frames_of_all_Stocks_Post_Data_Processing['TIMESTAMP'] == self.Get_Today_Date()) & (
                                    data_frames_of_all_Stocks_Post_Data_Processing['COI_Percentage'] > float(values['_COI_INPUT_Value_']))]
                    filtered_Data_frame.to_csv("FilteredData.csv")

                elif (values['Percentage_volume_enabled'] & (not values['COI_Percentage_enabled'])):

                    data_frames_of_all_Stocks_Post_Data_Processing = pd.read_csv("Koko.csv")

                    filtered_Data_frame = data_frames_of_all_Stocks_Post_Data_Processing[
                        (data_frames_of_all_Stocks_Post_Data_Processing['TIMESTAMP'] == self.Get_Today_Date()) & (
                                data_frames_of_all_Stocks_Post_Data_Processing['Per_vol_Updown'] > float(values[
                            '_Percentage_SLIDER_Value_']))]
                    filtered_Data_frame.to_csv("FilteredData.csv")

                elif (values['Percentage_volume_enabled'] & (values['COI_Percentage_enabled'])):

                    data_frames_of_all_Stocks_Post_Data_Processing = pd.read_csv("Koko.csv")

                    filtered_Data_frame = data_frames_of_all_Stocks_Post_Data_Processing[(data_frames_of_all_Stocks_Post_Data_Processing['TIMESTAMP'] == self.Get_Today_Date()) & (data_frames_of_all_Stocks_Post_Data_Processing['Per_vol_Updown'] > float(values['_Percentage_SLIDER_Value_'])) & (data_frames_of_all_Stocks_Post_Data_Processing['COI_Percentage'] > float(values[
                                '_COI_INPUT_Value_']))]
                    print("uepp0 ;",self.Get_Today_Date())
                    filtered_Data_frame.to_csv("FilteredData.csv")

            elif (event_Tabel_FO_Data == gui.WIN_CLOSED):
                    break
        window_Tabel_FO_Data.close()






