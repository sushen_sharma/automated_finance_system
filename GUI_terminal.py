import PySimpleGUI as gui
from PySimpleGUI import WIN_CLOSED
from Trading_Setup_GUI.Event_DownloadData import  Event_Operation
from Trading_Setup_GUI.Folder_operations import Folder_Operation
import  pandas as pd
import csv

#Creating Objects for imported Class
Event_Operation_object = Event_Operation()

from win32api import GetSystemMetrics  #GetSystemMetrics is used to get Currently set Screen Resolution ,so that application window should open accordingly the screen size
gui.theme('DarkBlue3')

menu_def = [['Setup Type',['Intraday','Swing Trading']]]

layout1 = [
           [gui.Button("Intraday Setup",font=('MS Sans Serif', 10, 'bold'),button_color=('red', 'white'))],
           [gui.Button("Swing Trading Setup", font=('MS Sans Serif', 10, 'bold'), button_color=('red', 'white'))]
          ]

layout = [
            [   gui.Button("Download",font=('MS Sans Serif', 10, 'bold'),button_color=('red', 'white')),
                gui.Button("Analysis",font=('MS Sans Serif', 10, 'bold'),button_color=('red', 'white')),
                gui.Button("Configuration",font=('MS Sans Serif', 10, 'bold'),button_color=('red', 'white')),
                gui.Button("Read Logs",font=('MS Sans Serif', 10, 'bold'),button_color=('red', 'white')),
                gui.Button("Exit",font=('MS Sans Serif', 10, 'bold'),button_color=('red', 'white'))
            ]


          ]



window = gui.Window('Bramahastra',layout,size=[GetSystemMetrics(0),GetSystemMetrics(1)])

while True:
    event,values = window.read()
    print("event",event)
    print("values",values)
    if (event == gui.WIN_CLOSED) or (event == "Exit"):
        break
    elif event == "Download":
        Event_Operation_object.download()
        gui.popup_ok("Data Downloaded",background_color="black",text_color="yellow")
    elif event == "Read Logs":
        gui.popup_scrolled(Event_Operation_object.ReadLogs())
    elif event == "Configuration":
        gui.popup_ok("Configuration",background_color="black",text_color="yellow")
    elif event == "Analysis":
        window1 = gui.Window('Bramahastra', layout1, size=[GetSystemMetrics(0), GetSystemMetrics(1)])
        while True:
            event_sub_window, values_sub_window = window1.read()
            if event_sub_window == gui.WIN_CLOSED:
                 break
            if event_sub_window == "Intraday Setup":
                Event_Operation_object.Promoter_Data()
            elif event_sub_window == "Swing Trading Setup":
                Event_Operation_object.Show_Fo_bhav_Copy()
        window1.close()

window.close()
