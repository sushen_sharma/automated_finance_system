import requests
import logging
import os
import csv
import PySimpleGUI as gui
from datetime import  date
import datetime
import  pandas as pd
from win32api import GetSystemMetrics
from Trading_Setup_GUI.Folder_operations import  Folder_Operation
from Trading_Setup_GUI.BusinessLogic import  Business_Logics
import plotly.graph_objects as go
import pandas as pd
import datetime



class Event_Operation():
    f = Folder_Operation()
    business_logic_object = Business_Logics()
    def __init__(self):
        Log_FilePath = Event_Operation.f.Create_Folder_to_Stor_ApplicationLogs_IfDoesnot_Exist()
        print(Log_FilePath)
        logging.basicConfig(filename=Log_FilePath+"/"+"ApplicationLogs.log",format='%(asctime)s %(levelname)-8s %(message)s',level=logging.DEBUG,datefmt='%d-%m-%Y %H:%M:%S')
        f = Folder_Operation()





    def download(self):
        today = date.today()
        x = datetime.datetime(today.year, today.month, today.day)
        Derivative_Date_Format = x.strftime("%d%b%Y").upper()
        Cash_Date_Format = x.strftime("%d%m%Y").upper()

        url_list  = ["https://archives.nseindia.com/content/historical/DERIVATIVES/2020/SEP/fo{0}bhav.csv.zip".replace("{0}",Derivative_Date_Format),"https://archives.nseindia.com/products/content/sec_bhavdata_full_{0}.csv".replace("{0}",Cash_Date_Format)]
        print(url_list)
        Parent_file_Path = Event_Operation.f.Create_DownloadFolder_ifDoesnot_Exist()
        for url in url_list:
            r = ''
            try:
                r = requests.get(url,allow_redirects=False) #allow_redirects set to False ,to avoid going to NSE 404 error page and then finding out file doesn't exist
            except:
                print("###########################################in Excpetion#########################################################")
                gui.popup_ok("Oops ! Some error occured ,Close The Application and Start Again")
                #self.download()
            if (r.status_code == 200):
                if (len(url.split("/")[-1]) > 0):
                    logging.info("File Exists and it is downloadable")
                    open(Parent_file_Path+"/"+url.split("/")[-1],'wb').write(r.content)
                    if "zip" in url.split("/")[-1]:
                      Event_Operation.f.unzip_operation(Parent_file_Path+"/"+url.split("/")[-1])
                else:
                    logging.debug("There is some Issue with Downloading URL or check with sushen to debug the issue")

            else:
                   logging.warning("File Not Downloadable,because It Doesn't Exist")

    def ReadLogs(self):
        Log_FilePath = Event_Operation.f.Create_Folder_to_Stor_ApplicationLogs_IfDoesnot_Exist()
        f = open(Log_FilePath+"/"+"ApplicationLogs.log", "r")
        text = f.read()
        return text

    def Show_Fo_bhav_Copy(self):
        list_of_FO_files_Downloaded = self.f.Downloaded_FO_Bhav_CopyFiles()
        list_of_Cash_files_Downloaded = self.f.Downloaded_Cash_Files()
        self.business_logic_object.Future_and_Cash_Data(list_of_FO_files_Downloaded,list_of_Cash_files_Downloaded)
        #Event_Operation.business_logic_object.Future_and_Cash_Data()

    def Promoter_Data(self):
        file_path = "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Promoter Data Swing Trading\\CF-Insider-Trading-equities-23-05-2020-to-23-08-2020.csv"
        data = pd.read_csv(file_path)
        # print(data.columns)
        category_of_Person = ['Promoter Group', 'Promoters']
        filtered_data = data[['SYMBOL \n', 'CATEGORY OF PERSON \n', 'VALUE OF SECURITY (ACQUIRED/DISPLOSED) \n',
                              'NO. OF SECURITIES (ACQUIRED/DISPLOSED) \n', 'MODE OF ACQUISITION \n']]
        required_data = filtered_data[(filtered_data['CATEGORY OF PERSON \n'].isin(category_of_Person)) & (
                    filtered_data['MODE OF ACQUISITION \n'] == 'Market Purchase')]

        required_data['VALUE OF SECURITY (ACQUIRED/DISPLOSED) \n'] = pd.to_numeric(
            required_data['VALUE OF SECURITY (ACQUIRED/DISPLOSED) \n'], errors="coerce").fillna(0)
        required_data['NO. OF SECURITIES (ACQUIRED/DISPLOSED) \n'] = pd.to_numeric(
            required_data['NO. OF SECURITIES (ACQUIRED/DISPLOSED) \n'], errors="coerce").fillna(0)

        d = required_data.groupby('SYMBOL \n')

        Result_dataFrame_valueOfSecurities = d['VALUE OF SECURITY (ACQUIRED/DISPLOSED) \n'].sum()
        Result_dataFrame_NumberOfSecurities = d['NO. OF SECURITIES (ACQUIRED/DISPLOSED) \n'].sum()
        Average_Price = Result_dataFrame_valueOfSecurities / Result_dataFrame_NumberOfSecurities

        Result_dataFrame_new = pd.concat(
            [Result_dataFrame_valueOfSecurities, Result_dataFrame_NumberOfSecurities, Average_Price], axis=1)
        Result_dataFrame_new.rename(columns={0: 'Average_Price'}, inplace=True)
        Result_dataFrame_new.round(3)
        shortlisted_data_frame = Result_dataFrame_new[
            Result_dataFrame_new['VALUE OF SECURITY (ACQUIRED/DISPLOSED) \n'] > 10000000]

        print(shortlisted_data_frame.sort_values(ascending=False, by='VALUE OF SECURITY (ACQUIRED/DISPLOSED) \n'))
        shortlisted_data_frame = required_data[['SYMBOL \n','VALUE OF SECURITY (ACQUIRED/DISPLOSED) \n']]
        shortlisted_data_frame.to_csv("Promoter_Data.csv")






